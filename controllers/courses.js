const Course = require('../models/Course');


//Create a new course
/*
Steps:
	1. Create a new Course object using the mongoose model and the information from the request body.
	2. Save the new Course to the database
*/
module.exports.addCourse =(reqBody) => {
	//create a variable "newCourse" and intantiate the name, description, price

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error) => {
		if (error) {
			return error;
		} else {
			return course;
		}
	}).catch(error => error.message);
};

//Retrieve all courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

//Retrieve all ACTIVE courses
//1.Retrieve all the courses with the propert isActive: true

module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	}).catch(error => error)
}

//Retrieving a SPECIFIC course
//1. Retrieve the course that matches the course ID provided from the URL

module.exports.getCourse =(reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result
	}).catch(error => error);
};

//UPDATE a course
/*
Steps:
	1. Create a variable "updatedCourse" which will contain the info retrieved from the req.body
	2. Find and Update the course using the courseId retrieved from the req.params and the variable "updatedCourse" containing info from req.body

*/

  module.exports.updateCourse = (courseId, data) => {
    //specify the fields/properties of the document to be updated
    let updatedCourse = {
      name: data.name,
      description: data.description,
      price: data.price
    }

    //findByIdAndUpdate(document Id, updatesToBeApplied)
    return Course.findByIdAndUpdate(courseId, updatedCourse).then((course, error) => {
      if(error){
        return false;
      } else {
        return true;
      }
    }).catch(error => error)
  };

// Archiving a course

	module.exports.archiveCourse = (courseId) => {
		let updateActiveField = {
			isActive : false
		};
		return Course.findByIdAndUpdate(courseId, updateActiveField).then((course, error) => {
			if (error) {
				return false
			} else {
				return true
			}
		}).catch(error => error)
	};