//[SECTION] Dependencies and Modules
	const User = require('../models/User');	
	const Course = require('../models/Course')
	const bcrypt = require('bcrypt');
	const dotenv = require('dotenv')
	const auth = require('../auth')

//[SECTION] Environment Setup
	dotenv.config();
	const salt = parseInt(process.env.SALT);

//[SECTION] Functionalites [CREATE]
	module.exports.register = (userData) => {
		let fName = userData.firstName;
		let lName = userData.lastName;
		let email = userData.email;
		let passW = userData.password;
		let mobil = userData.mobileNo;
		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(passW, salt),
			mobileNo: mobil
		})
		return newUser.save().then((user, err)=>{
			if (user) {
				return user
			} else {
				return {message:'Failed to Register account'};
			};
		});
	};

//User Authentication
/*
Steps:
1. Check the database if the user email exists
2. Compare the password provided in the login from the password stored in the database.
3. Generate/return a JSON web token if the user is successfully logged in, and return false if not
	
*/	
module.exports.loginUser = (data) => {
	return User.findOne({email: data.email}).then(result => {
		if(result == null){
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(data.password, result.password)
			if(isPasswordCorrect){
				return {accessToken: auth.createAccessToken(result.toObject())}
			} else {
				return false;
			};
		};
	});
};
//[SECTION]	Functionalites [RETRIEVE] the user details
/*
Steps.
1. Find the document in the database using the user's ID
2. Reaassign the password of the returned document to an empty string.
3. Return the result back to the client
*/
	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			result.password = '';
			return result;
		})
	}
//Enroll registered User
/*
ENROLLMENT STEPS:
	1.Look for the user by its ID.
		-push the details of the course we're trying to enroll in. We'll push the details to a new enrollment subdocument in our user.
	2. Look for the course by its ID.
		-push the details of the enrollee/user who's trying to enroll. We'll push to a new enrollees subdocument in our course.
	3. When both saving document are successful, we send a message to a client. true if successful, false is not	
*/
 
 module.exports.enroll = async (req,res) => {
 	// console.log('Test Enroll Route');
 	console.log(req.user.id);
 	console.log(req.body.courseId);

 	if (req.user.isAdmin){
 		return res.send({message: "Action Forbidden"})
 	}

 	//get the user's ID to save the courseID inside the enrollments field

 	let isUserUpdated = await User.findById(req.user.id).then(user => {
 		//add the courseId in an object and push that object into user's enrollments array:
 		let newEnrollment = {
 			courseId: req.body.courseId
 		}

 		user.enrollments.push(newEnrollment);
 		return user.save().then(user => true).catch(error => 
 			error.message)

 		//if isUserUpdated does not containt the boolean true, we will stop our process and return a message to our client
 		if (isUserUpdated !== true) {
 			return res.send({ message: isUserUpdated })
 		}
 	});
 	
 	//Find the course Id that we will need to push to our enrolllee

 	let isCourseUpdated = await Course.findById(req.body.courseId).then(course => {
 		let enrollee = {
 			userId: req.user.id
 		}

 		course.enrollees.push(enrollee);
 		return course.save().then(course => true).catch(error => error.message)

 		if (isCourseUpdated !== true){
 			return res.send({message: isCourseUpdated})
 		}
 	});

 	//send message to the client that we have successfully enerolled our user if both isUserUpdated and isCourseUpdated contain the boolean true

 	if (isUserUpdated && isCourseUpdated){
 		return res.send({meessage: "Enrolled Succesfully"})
 	}

 } 

