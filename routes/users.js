//[SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users');
	const auth = require('../auth')
//[SECTION]	Routing Component
	const route = exp.Router();
//[SECTION]	Routes POST
	route.post('/register', (req, res) => {
		let userData = req.body;
		controller.register(userData).then(result => {
			res.send(result)
		});
	});

//[SECTION]	Route for user Authentication(login)
	route.post('/login', (req, res) => {
		controller.loginUser(req.body).then(results => res.send(results));
	})

//[SECTION]	Routes GET the users details
	route.get('/details', auth.verify, (req, res) => {
		controller.getProfile(req.user.id).then(result => res.send(result));
	});

//ENROLL OUR REGISTERED USERS
//only the verified user can enroll in a course

route.post('/enroll', auth.verify, controller.enroll);
//[SECTION] Expose Route System
	module.exports = route;